package tn.esprit.spring;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;


import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.entities.Role;
import tn.esprit.spring.entities.Timesheet;
import tn.esprit.spring.services.EmployeServiceImpl;
import tn.esprit.spring.services.IEmployeService;
import tn.esprit.spring.repository.ContratRepository;
import tn.esprit.spring.repository.EmployeRepository;

@RunWith(SpringRunner.class)
@SpringBootTest

public  class EmployeServiceImplTest  {
	

	@Autowired
	IEmployeService uservice;
	//@Autowired
	//EmployeRepository employeRepository;
	@Autowired
	ContratRepository contratRepoistory;
	@Autowired
	IEmployeService employeservice ; 
	//@Autowired
	//EmployeServiceImpl employeServiceImpl;
	//@Autowired
	//Contrat contrat ;
	//@Autowired
	//Employe employe;
	@Test
	public void contextLoads() throws Exception {
		assertThat(employeservice).isNotNull();
	}

@Test
public void ajouterContratTest() {
	Contrat contrat = new Contrat(new Date(2020, 12, 01),"Travail",10.5f);
	contrat.setReference(2);
	 long initialSize = contratRepoistory.count();
	 employeservice.ajouterContrat(contrat);
	 long sizeAfterInsert  = contratRepoistory.count();
	 assertTrue(initialSize < sizeAfterInsert);

}

@Test
public void deleteContratByIdTest() {
	 long initialSize = contratRepoistory.count();
	 employeServiceImpl.deleteContratById(2);
	 long sizeAfterInsert  = contratRepoistory.count();
	 assertTrue(initialSize > sizeAfterInsert);	
   
}
}



