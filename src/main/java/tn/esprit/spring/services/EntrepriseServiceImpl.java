package tn.esprit.spring.services;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import java.util.Collections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EntrepriseRepository;

@Service
public class EntrepriseServiceImpl implements IEntrepriseService {
    private static final Logger LOG = LogManager.getLogger(EntrepriseServiceImpl.class);

    @Autowired
    EntrepriseRepository entrepriseRepoistory;
    @Autowired
    DepartementRepository deptRepoistory;

    public int ajouterEntreprise(Entreprise entreprise) {
    	LOG.info(entreprise);  
        entrepriseRepoistory.save(entreprise);
        LOG.debug( entreprise);
        LOG.debug(entreprise.getId());
        return entreprise.getId();
    }

    public int ajouterDepartement(Departement dep) {
        LOG.info(dep);
        deptRepoistory.save(dep);
        LOG.debug(dep);
        LOG.debug( dep.getId());
        return dep.getId();
    }

    public void affecterDepartementAEntreprise(int depId, int entrepriseId) {
        // Le bout Master de cette relation N:1 est departement
        // donc il faut rajouter l'entreprise a departement
        // ==> c'est l'objet departement(le master) qui va mettre a jour
        // l'association
        // Rappel : la classe qui contient mappedBy represente le bout Slave
        // Rappel : Dans une relation oneToMany le mappedBy doit etre du cote
        // one.
        LOG.info( entrepriseId);
		LOG.info(depId);
		Entreprise entr = null ;
		Departement dep = null;
		Optional<Entreprise> entreprise =entrepriseRepoistory.findById(entrepriseId);
		Optional<Departement> department =deptRepoistory.findById(depId);
		if (entreprise.isPresent()&&department.isPresent()) {
            entr = entreprise.get();
            LOG.debug(entrepriseId);

            dep = department.get();
            LOG.debug(depId);
            dep.setEntreprise(entr);	

		LOG.info( entrepriseId);
		deptRepoistory.save(dep);
		LOG.info( dep);
        } else {
            LOG.info("Entreprise or Departement doesn't exist");
        }
    }

    public List<String> getAllDepartementsNamesByEntreprise(int entrepriseId) {
        LOG.info( entrepriseId);
        List<String> depNames = new ArrayList<>();
		Entreprise entr = null ;
		Optional<Entreprise> entreprise =entrepriseRepoistory.findById(entrepriseId);
		if (entreprise.isPresent()) {
            entr = entreprise.get();
            LOG.debug(entrepriseId);
            for (Departement dep : entr.getDepartements()) {
    			depNames.add(dep.getName());
    		}
    		LOG.debug( depNames);
    		return depNames;
        } else {
        	return Collections.emptyList(); 
        }
    }

    @Transactional
    public void deleteEntrepriseById(int entrepriseId) {
    	Entreprise entreprise = null;
    	LOG.info(entrepriseId);
    	Optional<Entreprise> entR = entrepriseRepoistory.findById(entrepriseId);
        if (entR.isPresent()) {
        	entreprise = entR.get();
        	entrepriseRepoistory.delete(entreprise);
        	LOG.debug( entrepriseId);
        }
    }

    @Transactional
    public void deleteDepartementById(int depId) {
        LOG.info( depId);
		Departement dep = null;
		Optional<Departement> department =deptRepoistory.findById(depId);
        if (department.isPresent()) {
            dep = department.get();
            LOG.debug(depId);
    		deptRepoistory.delete(dep);
            LOG.debug(depId);

        }
    }

	public Entreprise getEntrepriseById(int entrepriseId) {
		Entreprise entreprise = null;
		LOG.info(entrepriseId);
		Optional<Entreprise> entR = entrepriseRepoistory.findById(entrepriseId);
		if (entR.isPresent()) {
			entreprise = entR.get();
		}
		LOG.debug(entreprise);
		return entreprise;
	}

}
 