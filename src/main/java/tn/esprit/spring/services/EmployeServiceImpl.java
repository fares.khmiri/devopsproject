package tn.esprit.spring.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.entities.Timesheet;
import tn.esprit.spring.repository.ContratRepository;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EmployeRepository;
import tn.esprit.spring.repository.TimesheetRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Service
public class EmployeServiceImpl implements IEmployeService {

	@Autowired
	EmployeRepository employeRepository;
	@Autowired
	DepartementRepository deptRepoistory;
	@Autowired
	ContratRepository contratRepoistory;
	@Autowired
	TimesheetRepository timesheetRepository;

	Logger loggerA = LogManager.getLogger(EmployeServiceImpl.class);

	public int ajouterEmploye(Employe employe) {
		loggerA.info("*****save employe*****");
		employeRepository.save(employe);
		loggerA.info("*****Employe saved with success*****");
		return employe.getId();
	}  

	//me
	public void mettreAjourEmailByEmployeId(String email, int employeId) {
		   loggerA.info("Start mettreAjourEmailByEmployeId");
			Employe employe = employeRepository.findById(employeId).get();
			employe.setEmail(email);
			employeRepository.save(employe);
			loggerA.info("End mettreAjourEmailByEmployeId");
			
		}

    @Transactional
	public void affecterEmployeADepartement(int employeId, int depId) {
		Optional<Departement> depManagedEntity = deptRepoistory.findById(depId);
		Optional<Employe> employeManagedEntity = employeRepository.findById(employeId);
		Departement dep;
		Employe emp = new Employe();

		if (depManagedEntity.isPresent()) {

			dep = depManagedEntity.get();
			if (dep.getEmployes() == null) {

				if (employeManagedEntity.isPresent()) {
					emp = employeManagedEntity.get();
					List<Employe> employes = new ArrayList<>();
					employes.add(emp);
					dep.setEmployes(employes);
				}

			} else {

				dep.getEmployes().add(emp);
			}
			loggerA.info("*****Employe added successfully to Departement*****");
			deptRepoistory.save(dep);

		}
	}
    @Transactional
	public void desaffecterEmployeDuDepartement(int employeId, int depId)
	{
		Departement dep = deptRepoistory.findById(depId).get();

		int employeNb = dep.getEmployes().size();
		for(int index = 0; index < employeNb; index++){
			if(dep.getEmployes().get(index).getId() == employeId){
				dep.getEmployes().remove(index);
				break;//a revoir
			}
		}
	}

	//mee
	public int ajouterContrat(Contrat contrat) {
		loggerA.info("Start ajouterContrat");
		contratRepoistory.save(contrat);
		loggerA.info("Contrat added Successfully");
		loggerA.error("Add contrat failed");
		  return contrat.getReference();
	}

	//me
	public void affecterContratAEmploye(int contratId, int employeId) {
		loggerA.info("Start affecterContratAEmploye");
		loggerA.debug(contratId);
		try {
			Contrat contratManagedEntity = contratRepoistory.findById(contratId).get();
			Employe employeManagedEntity = employeRepository.findById(employeId).get();
			contratManagedEntity.setEmploye(employeManagedEntity);
			contratRepoistory.save(contratManagedEntity);
			loggerA.info("End affecterContratAEmploye");
		}catch (Exception e) {
			// TODO: handle exception
			loggerA.error("affecter Contra At Employe failed",e);
		}

	}
   public String getEmployePrenomById(int employeId) {
		loggerA.info("***start get Employe Prenom By Id");
		Optional<Employe> employeManagedEntity = employeRepository.findById(employeId);
		Employe emp = new Employe();
		if (employeManagedEntity.isPresent()) {
			emp = employeManagedEntity.get();
		}
		return emp.getPrenom();
	}

   public void deleteEmployeById(int employeId) {
		Employe employe = null;
		Optional<Employe> emp = employeRepository.findById(employeId);
		if (emp.isPresent()) {
			employe = emp.get();
			// Desaffecter l'employe de tous les departements
			// c'est le bout master qui permet de mettre a jour
			// la table d'association
			for (Departement dep : employe.getDepartements()) {
				dep.getEmployes().remove(employe);
			}
			loggerA.info("***Employe deleted with success***");
			employeRepository.delete(employe);
		}
	}

	//me
	public void deleteContratById(int contratId) {
		loggerA.info("Start deleteContratById");
		loggerA.debug(contratId );
			Contrat contratManagedEntity = contratRepoistory.findById(contratId).get();
			contratRepoistory.delete(contratManagedEntity);
			loggerA.info("Contrat id" + contratRepoistory.findById(contratId).get());
			loggerA.info("Contrat deleted Successfully");
			loggerA.error("Delete contrat failed");
		}
	//me
	public int getNombreEmployeJPQL() {
		loggerA.info("Start getNombreEmployeJPQ");
		loggerA.info("Liste employees" + employeRepository.countemp());
		int count= employeRepository.countemp();
		loggerA.info("End getNombreEmployeJPQ");
		loggerA.error("Cannot find NombreEmployeJPQL");
		return count;
	}

	
	public List<String> getAllEmployeNamesJPQL() {
		return employeRepository.employeNames();

	}
	
	public List<Employe> getAllEmployeByEntreprise(Entreprise entreprise) {
		return employeRepository.getAllEmployeByEntreprisec(entreprise);
	}

	public void mettreAjourEmailByEmployeIdJPQL(String email, int employeId) {
		employeRepository.mettreAjourEmailByEmployeIdJPQL(email, employeId);
		loggerA.info("***Employe's email updated with success***");

	}
	public void deleteAllContratJPQL() {
         employeRepository.deleteAllContratJPQL();
	}
	
	public float getSalaireByEmployeIdJPQL(int employeId) {
		return employeRepository.getSalaireByEmployeIdJPQL(employeId);
	}

	public Double getSalaireMoyenByDepartementId(int departementId) {
		return employeRepository.getSalaireMoyenByDepartementId(departementId);
	}
	
	public List<Timesheet> getTimesheetsByMissionAndDate(Employe employe, Mission mission, Date dateDebut,
			Date dateFin) {
		loggerA.info("***get timesheets By Mission And Date***");
		return timesheetRepository.getTimesheetsByMissionAndDate(employe, mission, dateDebut, dateFin);
		
	}

	//me
	public List<Employe> getAllEmployes() {
		loggerA.info("Start getAllEmployes()");
		List<Employe> list= (List<Employe>) employeRepository.findAll();
		loggerA.info("End getAllEmployes=()");
		loggerA.error("Cannot find liste Employes");
	
		return list;

	}

}
